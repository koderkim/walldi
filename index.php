<?php get_header(); ?>
		<?php $categories = array(13,15,17,19,21); ?>
		<?php
			if(class_exists('Polylang')):
				$categoriesPll = array();
				foreach($categories as $category){
					$term_id = pll_get_term($category);
					if($term_id){
						$categoriesPll[] = $term_id;
					}
				}
				$categories = $categoriesPll;
			endif;
		?>
		<div>
			<div id="fullpage">
				<?php foreach($categories as $category): ?>
				<div class="front_pannel section" style="background-image:url(<?php echo wp_get_attachment_image_url(get_field('background', 'category_'.$category), 'full'); ?>)">
					<div class="container">
						<div>
							<h2 class="front_pannel__title"><a href="<?php echo get_category_link($category); ?>"><?php echo get_cat_name($category); ?></a></h2>
							<div class="front_pannel__description"><?php echo category_description($category); ?></div>
							<div class="front_pannel__link"><a href="<?php echo get_category_link($category); ?>" class="btn btn__link"><?php _e('more', 'walldi'); ?></a></div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</div>

<script>
	var scrollPos = 0;
	var is_scrolling = false;

	Math.easeInOutQuad = function(t, b, c, d){
		t /= d/2;
		if (t < 1) {
			return c/2*t*t + b
		}
		t--;
		return -c/2 * (t*(t-2) - 1) + b;
	};

	Math.easeInCubic = function(t, b, c, d){
		var tc = (t/=d)*t*t;
		return b+c*(tc);
	};

	Math.inOutQuintic = function(t, b, c, d){
		var ts = (t/=d)*t,
		tc = ts*t;
		return b+c*(6*tc*ts + -15*ts*ts + 10*tc);
	};

	var requestAnimFrame = (function(){
		return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback){window.setTimeout(callback, 1000/60);};
	})();

	function scrollTo(to, callback, duration) {
		function move(amount){
			document.documentElement.scrollTop = amount;
			document.body.parentNode.scrollTop = amount;
			document.body.scrollTop = amount;
		}
		function position(){
			return document.documentElement.scrollTop || document.body.parentNode.scrollTop || document.body.scrollTop;
		}
		var start = position(),
			change = to - start,
			currentTime = 0,
			increment = 20;
			duration = (typeof(duration) === 'undefined') ? 500 : duration;
		var animateScroll = function(){
			currentTime += increment;
			var val = Math.inOutQuintic(currentTime, start, change, duration);
			move(val);
			if(currentTime < duration){
				requestAnimFrame(animateScroll);
			} else {
				if(callback && typeof(callback) === 'function'){
					callback();
				}
			}
		};
		animateScroll();
	}

	window.addEventListener('scroll', function(event){
		if(document.body.clientWidth > 767){
			var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
			var front_panel = document.querySelectorAll('.front_pannel');
			if(front_panel && !is_scrolling){
				if(scrollPos < scrollTop){
					var panelActive = -1;
					for(var i=0; i<front_panel.length; i++){
						var panel_offset = front_panel[i].getBoundingClientRect().top + window.scrollY;
						if(panel_offset > scrollTop){
							panelActive = i;
							break;
						}
					}
					if(panelActive > -1){
						is_scrolling = true;
						window.scrollTo(panel_offset, setTimeout(
							function(){
								is_scrolling = false;
							}, 1000)
						);
					}
				} else {
					var panelActive = -1;
					for(var i=front_panel.length-1; i>=0; i--){
						var panel_offset = front_panel[i].getBoundingClientRect().top + window.scrollY;
						if(panel_offset < scrollTop){
							panelActive = i;
							break;
						}
					}
					if(panelActive > -1){
						is_scrolling = true;
						window.scrollTo(panel_offset, setTimeout(
							function(){
								is_scrolling = false;
							}, 1000)
						);
					}
				}
			}
			scrollPos = scrollTop;
		}
	});
</script>

		<?php if($description = get_field('description', pll_current_language())): ?>
		<div class="category__additional-text">
			<div class="container">
				<div class="category__additional-text_wrap short">
					<?php echo $description; ?>
				</div>
				<div class="category__additional-text_wrap-btn" data-btn-text="<?php _e('Скрыть', 'walldi'); ?>"><?php _e('Подробнее', 'walldi'); ?></div>
			</div>
		</div>
		<script>
			window.addEventListener('DOMContentLoaded', function(){
				var btn = document.querySelector('.category__additional-text_wrap-btn');
				if(btn){
					var wrap = document.querySelector('.category__additional-text_wrap');
					if(wrap){
						btn.addEventListener('click', function(){
							wrap.classList.toggle('short');
							var btn_text = btn.innerHTML;
							btn.innerHTML = btn.getAttribute('data-btn-text');
							btn.setAttribute('data-btn-text', btn_text);
						});
					}
				}
			});
		</script>
		<?php endif; ?>
		<?php /* ?><section id="insta-widget">
			<div class="container">
				<div class="section-title"><?php _e('Мы в Instagram', 'walldi'); ?></div>
			</div>
			<div id="insta-widget__content">
				<?php echo do_shortcode('[instagram-feed]'); ?>
			</div>
		</section><?php */ ?>
<?php get_footer(); ?>