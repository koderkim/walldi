(function(){
	if(!Element.prototype.matches){
		Element.prototype.matches = Element.prototype.matchesSeleector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelctor
	}
})();

(function(){
	if(!Element.prototype.closest){
		Element.prototype.closest = function(css){
			var node = this;
			while(node){
				if(node.matches(css)) return node;
				else node = node.parentElement;
			}
			return null;
		}
	}
})();

window.addEventListener('DOMContentLoaded', function(){
	var header = document.querySelector('#header');
	var catalogmenu = document.querySelector('#catalogmenu');
	var menumin = document.querySelector('#menumin');
	if(catalogmenu && menumin){
		menumin.addEventListener('click', function(){
			header.classList.toggle('open');
			catalogmenu.classList.toggle('open');
		});
	}

	var tabItems = document.querySelectorAll('.tab__item-title');
	if(tabItems){
		for(var i=0; i<tabItems.length; i++){
			tabItems[i].addEventListener('click', function(){
				this.closest('.tab__item').classList.toggle('open');
			});
		}
	}

	var lang = document.querySelectorAll('.lang');
	if(lang){
		for(var i=0; i<lang.length; i++){
			lang[i].addEventListener('click', function(){
				this.classList.toggle('open');
			});
		}
	}

	var callback = document.querySelector('#callback');
	if(callback){
		callback.querySelector('.callback-title').innerHTML = translate_text.callback__title;
		callback.querySelector('input[name=user-name]').placeholder = translate_text.callback__name;
		callback.querySelector('input[name=user-phone]').placeholder = translate_text.callback__phone;
		callback.querySelector('textarea[name=user-message]').placeholder = translate_text.callback__message;
		callback.querySelector('input[type=submit]').value = translate_text.callback__send;
	}
});

window.addEventListener('scroll', function(){
	var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
	var header = document.querySelector('header');
	if(scrollTop > 100){
		header.classList.add('is-scrolling');
	} else {
		header.classList.remove('is-scrolling');
	}
});

function rebuildMenu(){
	var w = document.body.clientWidth;
	var mainmenu = document.querySelector('#mainmenu .top-menu');
	var catalogmenu = document.querySelector('#catalogmenu');
	if(mainmenu && catalogmenu){
		if(document.querySelector('#catalogmenu .top-menu')){
			Array.prototype.forEach.call(document.querySelectorAll('#catalogmenu .top-menu'), function(node){
				node.parentNode.removeChild(node);
			});
		}
		if(w < 768){
			catalogmenu.appendChild(mainmenu.cloneNode(true));
		}
	}
}

rebuildMenu();

window.addEventListener('resize', function(){
	rebuildMenu();
});

var price_request = {};
price_request.form = document.querySelector('#price_request');
price_request.open = function(){
	if(this.form){
		this.form.querySelector('.overlay-title').innerHTML = translate_text.price_request__title;
		this.form.querySelector('input[name=user-name]').placeholder = translate_text.price_request__name;
		this.form.querySelector('input[name=user-phone]').placeholder = translate_text.price_request__phone;
		this.form.querySelector('input[name=user-city]').placeholder = translate_text.price_request__city;
		this.form.querySelector('input[name^=user-project]').nextSibling.innerHTML = translate_text.price_request__availability;
		this.form.querySelector('input[name^=user-mounting]').nextSibling.innerHTML = translate_text.price_request__installation;
		this.form.querySelector('textarea[name=user-message]').placeholder = translate_text.price_request__message;
		this.form.querySelector('input[type=submit]').value = translate_text.price_request__send;
		this.form.classList.add('open');
	}
}
price_request.close = function(){
	if(this.form){
		this.form.classList.remove('open');
	}
}