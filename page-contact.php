<?php
// Template name: Contact
?>
<?php get_header(); ?>
		<div class="container">
			<h1 class="page-title"><?php _e('Contacts', 'walldi'); ?></h1>
		</div>

		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<p>
							<strong><?php _e('Уважаемые клиенты, просьба заблаговременно согласовать Ваш визит по нижеуказанным телефонам!', 'walldi'); ?></strong><br>
							<?php
								if($phones = get_theme_mod('walldi_phone')):
									$phones = explode("\n", $phones);
									foreach($phones as $phone):
							?>
							<a href="tel:<?php echo preg_replace('/[^0-9+]/', '', $phone); ?>"><?php echo $phone; ?></a><br>
							<?php endforeach; ?>
							<?php endif; ?>
							<?php if($email = get_theme_mod('walldi_email')): ?>
							<strong><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></strong>
							<?php endif; ?>
						</p>
						<?php if($address = get_theme_mod('walldi_address')): ?>
						<p>
							<strong>Шоу-рум:</strong> <?php echo nl2br($address); ?>
						</p>
						<?php endif; ?>
					</div>
					<div class="col-md-6">
						<img src="<?php echo get_template_directory_uri(); ?>/images/contact.png" alt="contact" class="img-responsive">
					</div>
				</div>
			</div>
		</section>

		<?php if($map = get_theme_mod('walldi_map')): ?>
		<section>
			<?php echo $map; ?>
		</section>
		<?php endif; ?>
<?php get_footer(); ?>