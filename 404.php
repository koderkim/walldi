<?php get_header(); ?>
	<main>
		<div class="container">
			<h1 class="page-title">404</h1>
		</div>
		
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php _e('Page not found', 'walldi'); ?>
					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>