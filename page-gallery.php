<?php
// Template name: Gallery
?>
<?php get_header(); ?>
	<main>
	<?php if(have_posts()): ?>
		<?php while(have_posts()): the_post(); ?>
		<div class="container">
			<h1 class="page-title"><?php the_title(); ?></h1>
		</div>
		
		<section>
			<div class="row">
				<div class="col-md-12">
					<?php the_content(); ?>
				</div>
			</div>
		</section>
		<?php endwhile; ?>
	<?php endif; ?>
	</main>
<?php get_footer(); ?>