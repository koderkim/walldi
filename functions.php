<?php
if(function_exists('add_theme_support')){
  add_theme_support('post-thumbnails');
  //add_theme_support('woocommerce');
}

remove_action('wp_head', 'wp_generator');

//require_once 'seo.php';

function setup_theme(){
  load_theme_textdomain('walldi', get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'setup_theme');

function translitLang($slug){
  $langs = array(
    'uk' => 'UKR',
    'ru' => 'RUS',
    'en' => 'ENG',
    'pl' => 'POL'
  );
  return isset($langs[$slug]) ? $langs[$slug] : $slug;
}

/*
// WP admin-ajax.php
function jsAjaxUrl(){
  $vars = array(
    'ajaxUrl' => admin_url('admin-ajax.php'),
    'is_mobile' => wp_is_mobile()
  );
  echo 
    '<script type="text/javascript">window.wpData = '.
    json_encode($vars).
    ';</script>';
}
add_action('wp_head', 'jsAjaxUrl');

function get_request_callback(){
  echo(json_encode( array('status'=>'ok','request_vars'=>$_REQUEST) ));
  wp_die();
}

add_action('wp_ajax_get_request', 'get_request_callback');
add_action('wp_ajax_nopriv_get_request', 'get_request_callback');
// End WP admin-ajax.php
*/

function wp_enqueue_method(){
  wp_enqueue_style('grid', get_template_directory_uri() . '/css/grid.min.css', array(), null);
  wp_enqueue_style('stylesheet', get_template_directory_uri() . '/css/stylesheet.min.css', array(), null);
  wp_dequeue_style('wp-block-library');
  wp_enqueue_script('main-common', get_template_directory_uri().'/js/common.js', array(), null, true);
}    
add_action('wp_enqueue_scripts', 'wp_enqueue_method');

register_nav_menus(array(
  'mainmenu' => 'Главное меню',
  'categorymenu' => 'Меню категорий',
  'footermenu1' => 'Меню подвала 1',
  'footermenu2' => 'Меню подвала 2',
));

if(function_exists('register_sidebar')){
  register_sidebar(array(
    'name' => 'Footer column 1',
    'id' => 'footer1',
    'before_widget' => '<div class="footermenu__list">',
    'after_widget' => '</div>',
    'before_title' => '<div class="footermenu__title">',
    'after_title' => '</div>'
  ));
  register_sidebar(array(
    'name' => 'Footer column 2',
    'id' => 'footer2',
    'before_widget' => '<div class="footermenu__list">',
    'after_widget' => '</div>',
    'before_title' => '<div class="footermenu__title">',
    'after_title' => '</div>'
  ));
};

/*
// gzip
function enable_zlib(){
    ini_set('zlib.output_compression', 'On');
    ini_set('zlib.output_compression_level', '1');
}
add_action('init', 'enable_zlib');
*/

function walldi_customizer_init($wp_customize){
  $wp_customize->add_section(
    'walldi_options',
    array(
      'title'     => 'Настройки сайта',
      'priority'  => 200,
    )
  );
  // Address
  $wp_customize->add_setting(
    'walldi_address',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'walldi_address',
    array(
      'section'  => 'walldi_options',
      'label'    => 'Address',
      'type'     => 'textarea'
    )
  );
  // Email
  $wp_customize->add_setting(
    'walldi_email',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'walldi_email',
    array(
      'section'  => 'walldi_options',
      'label'    => 'E-mail',
      'type'     => 'text'
    )
  );
  // Phone
  $wp_customize->add_setting(
    'walldi_phone',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'walldi_phone',
    array(
      'section'  => 'walldi_options',
      'label'    => 'Phone',
      'type'     => 'textarea',
      'description' => 'one phone per line'
    )
  );
  // Facebook
  $wp_customize->add_setting(
    'walldi_social_fb',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'walldi_social_fb',
    array(
      'section'  => 'walldi_options',
      'label'    => 'Facebook',
      'type'     => 'text'
    )
  );
  // Instagram
  $wp_customize->add_setting(
    'walldi_social_in',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'walldi_social_in',
    array(
      'section'  => 'walldi_options',
      'label'    => 'Instagram',
      'type'     => 'text'
    )
  );
  // Map
  $wp_customize->add_setting(
    'walldi_map',
    array(
      'default'   => '',
      'transport' => 'postMessage'
    )
  );
  $wp_customize->add_control(
    'walldi_map',
    array(
      'section'  => 'walldi_options',
      'label'    => 'Map',
      'type'     => 'textarea'
    )
  );
}
add_action('customize_register', 'walldi_customizer_init');

if('disable_gutenberg'){
  add_filter('use_block_editor_for_post_type', '__return_false', 100);
  add_action('admin_init', function(){
    remove_action('admin_notices', ['WP_Privacy_Policy_Content', 'notice']);
    add_action('edit_form_after_title', ['WP_Privacy_Policy_Content', 'notice']);
  });
}

/*function shortcode_func($atts, $content=""){
   return 'shortcode';
}
add_shortcode('attension', 'shortcode_func');*/

/*функция удаления ссылок с картинок в постах*/
// add_filter( ‘the_content’, ‘attachment_image_link_remove_filter’ );
// function attachment_image_link_remove_filter( $content ) {
//   $content = preg_replace(array('{<a(.*?)(wp-att|wp-content\/uploads)[^>]*><img}','{ wp-image-[0-9]*» /></a>}'), array('<img','» />'), $content);
//   return $content;
// }

// RULES
// add_filter("category_link", "filter_category_link");
// function filter_category_link($termlink){
//   if(preg_match("/\?cat=/", $termlink))
//     return $termlink;
//   $str = explode("/", $termlink);
//   $myslug = $str[count($str)-1];
//   $cat_id = get_category_by_slug($myslug);
//   $cat_id = $cat_id->category_parent;
//   $product_link = $str[4];
//   if($cat_id == 0) $termlink = preg_replace("/category.*?".$myslug."/", $myslug, $termlink);
//   else $termlink = preg_replace("/category.*?".$myslug."/", $product_link."/".$myslug, $termlink);
//   return $termlink;
// }

// add_filter("post_link", "filter_post_link", 10, 3);
// function filter_post_link($permalink, $post, $leavename){
//   if(preg_match("/\?p=/", $permalink))
//     return $permalink;
//   $str = explode("/", $permalink);
//   $postname = $str[count($str)-1];
//   $postcat = $str[count($str)-2];
//   $product_link = $str[3];
//   $permalink = preg_replace("/product.*?".$postname."/", $product_link."/".$postcat."/".$postname, $permalink);
//   return $permalink;
// }

// add_action('rewrite_rules_array', 'eg_add_rules_array');
// function eg_add_rules_array($wp_rewrite){
//   $rules = array();
//   $rules['broker/page/?([0-9]{1,})/?$'] = 'index.php?post_type=broker&paged=$matches[1]';
//   $rules['broker/(.+)/?$'] = 'index.php?broker=$matches[1]';
//   $rules['brokers/(.+)/?$'] = 'index.php?broker_cat=$matches[1]';
//   $rules['brokertags/(.+)/?$'] = 'index.php?broker_tag=$matches[1]';
//   $rules['product/([^/]+)/?$'] = 'index.php?category_name=$matches[1]';
//   $rules['product/([^/]+)/page/([0-9]{1,})/?$'] = 'index.php?category_name=$matches[1]&paged=$matches[2]';
//   $rules['product/([^/]+)/([^/]+)/?$'] = 'index.php?name=$matches[2]';

//   return $rules + $wp_rewrite;
// }

// add_filter('init','flushRules');
// function flushRules(){
//   global $wp_rewrite;
//   $wp_rewrite->flush_rules();
// }

// Колонка ID
if (is_admin()) {
  // колонка "ID" для таксономий (рубрик, меток и т.д.) в админке
  foreach (get_taxonomies() as $taxonomy){
    add_action("manage_edit-${taxonomy}_columns", 'tax_add_col');
    add_filter("manage_edit-${taxonomy}_sortable_columns", 'tax_add_col');
    add_filter("manage_${taxonomy}_custom_column", 'tax_show_id', 10, 3);
  }
  add_action('admin_print_styles-edit-tags.php', 'tax_id_style');
  function tax_add_col($columns) {return $columns + array ('tax_id' => 'ID');}
  function tax_show_id($v, $name, $id) {return 'tax_id' === $name ? $id : $v;}
  function tax_id_style() {print '<style>#tax_id{width:4em}</style>';}

  // колонка "ID" для постов и страниц в админке
  add_filter('manage_posts_columns', 'posts_add_col', 5);
  add_action('manage_posts_custom_column', 'posts_show_id', 5, 2);
  add_filter('manage_pages_columns', 'posts_add_col', 5);
  add_action('manage_pages_custom_column', 'posts_show_id', 5, 2);
  add_action('admin_print_styles-edit.php', 'posts_id_style');
  function posts_add_col($defaults) {$defaults['wps_post_id'] = __('ID'); return $defaults;}
  function posts_show_id($column_name, $id) {if ($column_name === 'wps_post_id') echo $id;}
  function posts_id_style() {print '<style>#wps_post_id{width:4em}</style>';}
}

/*function add_svg_upload_support($mimes = array()){
  $mimes['svg'] = "image/svg+xml";
  return $mimes;
}
add_action('upload_mimes', 'add_svg_upload_support');*/

/*function disable_comment_url($fields){ 
  unset($fields['url']);
  return $fields;
}
add_filter('comment_form_default_fields','disable_comment_url');

function move_comment_field_to_bottom($fields){
  $comment_field = $fields['comment'];
  unset( $fields['comment'] );
  $fields['comment'] = $comment_field;
  return $fields;
}
add_filter('comment_form_fields', 'move_comment_field_to_bottom');*/

// Comment reply
// function enqueue_comment_reply(){
//   if(is_singular() && comments_open() && (get_option('thread_comments') == 1)) 
//     wp_enqueue_script('comment-reply');
// }
// add_action('wp_enqueue_scripts', 'enqueue_comment_reply');

// Remove admin bar for user
/*function remove_admin_bar(){
  if(!current_user_can('administrator') && !is_admin()){
    show_admin_bar(false);
  }
}*/

/************************
// Тип данных Reviews
add_action('init', 'kim_reviews_init');
function kim_reviews_init()
{
  $labels = array(
  'name' => 'Отзывы', // Основное название типа записи
  'singular_name' => 'Отзыв', // отдельное название записи типа Book
  'add_new' => 'Добавить отзыв',
  'add_new_item' => 'Добавить новый отзыв',
  'edit_item' => 'Редактировать отзыв',
  'new_item' => 'Новый отзыв',
  'view_item' => 'Посмотреть отзыв',
  'search_items' => 'Найти отзыв',
  'not_found' =>  'Отзывов не найдено',
  'not_found_in_trash' => 'В корзине отзывов не найдено',
  'parent_item_colon' => '',
  'menu_name' => 'Отзывы'

  );
  $args = array(
  'labels' => $labels,
  'public' => true,
  'publicly_queryable' => true,
  'show_ui' => true,
  'show_in_menu' => true,
  'query_var' => true,
  'rewrite' => true,
  'menu_icon' => '
  dashicons-testimonial',
  'capability_type' => 'post',
  'has_archive' => true,
  'hierarchical' => false,
  'menu_position' => null,
  'supports' => array('title','editor','author','thumbnail','excerpt','comments')
  );
  register_post_type('reviews',$args);
}

// Добавляем фильтр, который изменит сообщение при публикации при изменении типа записи Book
add_filter('post_updated_messages', 'kim_reviews_updated_messages');
function kim_reviews_updated_messages($messages){
  global $post, $post_ID;

  $messages['reviews'] = array(
  0 => '', // Не используется. Сообщения используются с индекса 1.
  1 => sprintf('Отзыв обновлен. <a href="%s">Посмотреть отзыв</a>', esc_url(get_permalink($post_ID))),
  2 => 'Произвольное поле обновлено.',
  3 => 'Произвольное поле удалено.',
  4 => 'Отзыв обновлен.',
  // %s: дата и время ревизии
  5 => isset($_GET['revision']) ? sprintf('Отзыв восстановлен из ревизии %s', wp_post_revision_title((int) $_GET['revision'], false)) : false,
  6 => sprintf('Отзыв опубликован. <a href="%s">Перейти к отзыву</a>', esc_url(get_permalink($post_ID))),
  7 => 'Отзыв сохранен.',
  8 => sprintf('Отзыв сохранен. <a target="_blank" href="%s">Предпросмотр Отзыв</a>', esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
  9 => sprintf('Отзыв запланирована на: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Предпросмотр отзыва</a>',
    // Как форматировать даты в PHP можно посмотреть тут: http://php.net/date
    date_i18n(__('M j, Y @ G:i'), strtotime($post->post_date)), esc_url(get_permalink($post_ID))),
  10 => sprintf('Черновик записи Review обновлен. <a target="_blank" href="%s">Предпросмотр записи Review</a>', esc_url(add_query_arg('preview', 'true', get_permalink($post_ID)))),
  );

  return $messages;
}

// Разделы (рубрики) отзывов
add_action( 'init', 'create_kim_review_taxonomies', 0 );
function create_kim_review_taxonomies(){
  $labels = array(
  'name' => 'Разделы',
  'singular_name' => 'Разделы',
  'search_items' =>  'Искать Разделы',
  'popular_items' => 'Популярные Разделы',
  'all_items' => 'Все Разделы',
  'parent_item' => null,
  'parent_item_colon' => null,
  'edit_item' => 'Править Раздел',
  'update_item' => 'Обновить Раздел',
  'add_new_item' => 'Добавить новый раздел',
  'new_item_name' => 'Имя нового Раздела',
  'separate_items_with_commas' => 'Separate writers with commas',
  'add_or_remove_items' => 'Добавить или удалить Раздел',
  'choose_from_most_used' => 'Choose from the most used writers',
  'menu_name' => 'Разделы отзывов',
  );
// Добавляем древовидную таксономию 'Разделы' (как рубрики), чтобы сделать НЕ девовидную (как метки) значение для 'hierarchical' => false,
 
register_taxonomy('section', 'reviews', array(
  'hierarchical' => true,
  'labels' => $labels,
  'show_ui' => true,
  'query_var' => true,
  'rewrite' => array( 'slug' => 'section' ),
  ));
}

// Метки отзывов
add_action('init', 'create_kim_review_tags', 0);
function create_kim_review_tags(){
  $labels = array(
  'name' => 'Метка',
  'singular_name' => 'Метки',
  'search_items' =>  'Искать метки',
  'popular_items' => 'Популярные метки',
  'all_items' => 'Все метки',
  'parent_item' => null,
  'parent_item_colon' => null,
  'edit_item' => 'Править метку',
  'update_item' => 'Обновить метку',
  'add_new_item' => 'Добавить новую метку',
  'new_item_name' => 'Имя новой метки',
  'separate_items_with_commas' => 'Separate writers with commas',
  'add_or_remove_items' => 'Добавить или удалить метку',
  'choose_from_most_used' => 'Choose from the most used writers',
  'menu_name' => 'Метки отзывов',
  );
// Добавляем древовидную таксономию 'Разделы' (как рубрики), чтобы сделать НЕ девовидную (как метки) значение для 'hierarchical' => false,
 
register_taxonomy('rtags', 'reviews', array(
  'hierarchical' => false,
  'labels' => $labels,
  'show_ui' => true,
  'query_var' => true,
  'rewrite' => array( 'slug' => 'rtags'),
  ));
}

// Add fields after default fields above the comment box, always visible
add_action('comment_form_logged_in_after', 'additional_fields');
add_action('comment_form_after_fields', 'additional_fields');

function additional_fields(){
  echo '<p class="comment-form-title"><input id="review-title" name="review-title" class="title__review" type="text" placeholder="Заголовок" /></p>';
  echo '<p class="comment-form-type"><select id="review-type" class="select__type__reviews" name="review-type"><option value="like" selected>Положительный</option><option value="dislike">Отрицательный</option></select></p>';
}

add_action('comment_post', 'save_comment_meta_data');
function save_comment_meta_data($comment_id){
  if((isset($_POST['review-title'])) && ($_POST['review-title'] != ''))
  $review_title = wp_filter_nohtml_kses($_POST['review-title']);
  add_comment_meta($comment_id, 'review_title', $review_title);

  if((isset($_POST['review-type'])) && ($_POST['review-type'] != ''))
  $review_type = wp_filter_nohtml_kses($_POST['review-type']);
  add_comment_meta($comment_id, 'review_type', $review_type);
}
************************/

// add_filter('preprocess_comment', 'verify_comment_meta_data');
// function verify_comment_meta_data($commentdata){
//   if(!isset( $_POST['rating']))
//   wp_die(__('Error: You did not add a rating. Hit the Back button on your Web browser and resubmit your comment with a rating.'));
//   return $commentdata;
// }

/*
// Add the comment meta (saved earlier) to the comment text
// You can also output the comment meta values directly to the comments template  
add_filter('comment_text', 'modify_comment');
function modify_comment($text){

  if($commenttitle = get_comment_meta(get_comment_ID(), 'review_title', true)){
    $commenttitle = '<strong>'.esc_attr($commenttitle).'</strong><br/>';
    $text = $commenttitle.$text;
  }

  if($commenttitle = get_comment_meta(get_comment_ID(), 'review_type', true)){
    if(esc_attr($commenttitle) == 'like'):
      $text = $text."<br/><small>Положительный</small>";
    elseif(esc_attr($commenttitle) == 'dislike'):
      $text = $text."<br/><small>Отрицательный</small>";
    endif;
  }

  return $text;
}
*/

/*
// Add an edit option to comment editing screen  
add_action('add_meta_boxes_comment', 'extend_comment_add_meta_box');
function extend_comment_add_meta_box(){
  add_meta_box('title', __('Comment Metadata - Extend Comment'), 'extend_comment_meta_box', 'comment', 'normal', 'high');
}

function extend_comment_meta_box($comment){
    // $phone = get_comment_meta($comment->comment_ID, 'phone', true);
    $title = get_comment_meta($comment->comment_ID, 'review_title', true);
    $type = get_comment_meta($comment->comment_ID, 'review_type', true);
    wp_nonce_field('extend_comment_update', 'extend_comment_update', false);
    ?>
    <p>
        <label for="title">Заголовок</label>
        <input type="text" name="review-title" value="<?php echo esc_attr($title); ?>" class="title__review" />
    </p>
    <p>
      <select id="review-type" class="select__type__reviews" name="review-type">
        <option value="like"<?php if($type == 'like') echo " selected"; ?>>Положительный</option>
        <option value="dislike"<?php if($type == 'dislike') echo " selected"; ?>>Отрицательный</option>
      </select>
    </p>
    <?php
}

// Update comment meta data from comment editing screen 
add_action('edit_comment', 'extend_comment_edit_metafields');

function extend_comment_edit_metafields($comment_id){
  if(!isset($_POST['extend_comment_update']) || ! wp_verify_nonce($_POST['extend_comment_update'], 'extend_comment_update')) return;

  if((isset($_POST['review-title'])) && ($_POST['review-title'] != '')):
    $review_title = wp_filter_nohtml_kses($_POST['review-title']);
    update_comment_meta($comment_id, 'review_title', $review_title);
  else :
    delete_comment_meta($comment_id, 'review_title');
  endif;

  if((isset($_POST['review-type'])) && ($_POST['review-type'] != '')):
    $review_type = wp_filter_nohtml_kses($_POST['review-type']);
    update_comment_meta($comment_id, 'review_type', $review_type);
  else :
    delete_comment_meta($comment_id, 'review_type');
  endif;
}

// Страница отдельного комментария
add_filter('single_template', 'single_comments_template');
function single_comments_template($template){
  // global $wp_query;
  if($_GET['comments'] == 'full' and file_exists(TEMPLATEPATH.'/single-comments.php'))
    $template = TEMPLATEPATH.'/single-comments.php';
  return $template;
}
*/

// add http if not (ftp://, ftps://, http://, https://)
function addhttp($url){
  if(!preg_match("~^(?:f|ht)tps?://~i", $url)){
    $url = "http://".$url;
  }
  return $url;
}

// new line to paragraph
function nl2p($string) {
  $string_parts = explode("\n", $string);
  $string = '<p>' . implode('</p><p>', $string_parts) . '</p>'; 
  return str_replace("<p></p>", '', $string); 
}
/* or */
/*function nl2p($string){
  $paragraphs = '';
  foreach(explode("\n", $string) as $line){
    if(trim($line)){
      $paragraphs .= '<p>' . $line . '</p>';
    }
  }
  return $paragraphs;
}*/

/*
// Get all available values from ACF field
function get_meta_values($key = '', $type = 'post', $status = 'publish'){
    global $wpdb;
    
    if(empty($key)) return;
    
    $r = $wpdb->get_col($wpdb->prepare("
        SELECT DISTINCT(pm.meta_value) FROM {$wpdb->postmeta} pm
        LEFT JOIN {$wpdb->posts} p ON p.ID = pm.post_id
        WHERE pm.meta_key = '%s' 
        AND p.post_type = '%s'
        AND p.post_status = '%s' 
        ORDER BY pm.meta_value
      ", $key, $type, $status));
    
    return $r;
}*/

function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

function disable_emojis_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

add_filter('style_loader_tag', 'expert_remove_type_attr', 10, 2);
add_filter('script_loader_tag', 'expert_remove_type_attr', 10, 2);
function expert_remove_type_attr($tag, $handle){
  return preg_replace( "/ type=['\"]text\/(javascript|css)['\"]/", '', $tag );
}

/*
function application_ld_json(){
  global $post;
  
  ob_start();
  if(is_front_page()){
?>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
    {
      "@type": "ListItem",
      "position": 1,
      "item": 
      {
        "@id":"<?php echo home_url('/'); ?>",
        "name": "<?php echo get_bloginfo('name'); ?>"
      }
    },
    {
      "@type": "ListItem",
      "position": 2,
      "item": 
      {
        "@id":"<?php echo home_url('/'); ?>#",
        "name": "<?php echo get_bloginfo('description'); ?>"
      }
    }
  ]
}
</script>
<?php
  } elseif(is_category()){
    $categories = get_the_category($post->post_ID);
?>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
  {
    "@type": "ListItem",
    "position": 1,
    "item": 
    {
      "@id": "<?php echo home_url('/'); ?>",
      "name": "<?php echo get_bloginfo('name'); ?>"
    }
  },
  {
    "@type": "ListItem",
    "position": 2,
    "item": 
    {
      "@id": "<?php echo get_category_link($categories[0]->cat_ID); ?>",
      "name": "<?php echo $categories[0]->cat_name; ?>"
    }
  }]
}</script>
<?php
  } elseif(is_page()){
?>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
  {
    "@type": "ListItem",
    "position": 1,
    "item": 
    {
      "@id": "<?php echo home_url('/'); ?>",
      "name": "<?php echo get_bloginfo('name'); ?>"
    }
  },
  {
    "@type": "ListItem",
    "position": 2,
    "item": 
    {
      "@id": "<?php echo get_permalink($post->post_ID); ?>",
      "name": "<?php echo $post->post_title; ?>"
    }
  }]
}</script>
<?php
  } elseif(is_single()){
    $categories = get_the_category($post->post_ID);
?>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "Article",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?php echo get_the_permalink($post->post_ID); ?>"
  },
  "headline": "<?php echo addslashes(strip_tags(get_the_title($post->post_ID))); ?>",
  "description": "<?php echo addslashes(strip_tags(get_the_content(null, '', $post->post_ID))); ?>",
  <?php if(has_post_thumbnail($post->post_ID)): ?>
  "image": {
    "@type": "ImageObject",
    "url": "<?php echo get_the_post_thumbnail_url($post->post_ID); ?>",
    "width": 1795,
    "height": 1205
  },
  <?php endif; ?>
  "author": {
    "@type": "Organization",
    "name": "TellTrue"
  },  
  "publisher": {
    "@type": "Organization",
    "name": "TellTrue",
    "logo": {
      "@type": "ImageObject",
      "url": "<?php echo get_bloginfo('template_url'); ?>/images/logo.png",
      "width": 255,
      "height": 55
    }
  },
  "datePublished": "<?php echo get_the_date('Y-m-d', $post->post_ID); ?>",
  "dateModified": "<?php echo get_the_modified_date('Y-m-d', $post->post_ID); ?>"
}
</script>
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "BreadcrumbList",
  "itemListElement": [
  {
    "@type": "ListItem",
    "position": 1,
    "item": 
    {
      "@id": "<?php echo home_url('/'); ?>",
      "name": "<?php echo get_bloginfo('name'); ?>"
    }
  },
  {
    "@type": "ListItem",
    "position": 2,
    "item": 
    {
      "@id": "<?php echo get_category_link($categories[0]->cat_ID); ?>",
      "name": "<?php echo $categories[0]->cat_name; ?>"
    }
  },
  {
    "@type": "ListItem",
    "position": 3,
    "item": 
    {
      "@id": "<?php echo get_permalink($post->post_ID); ?>",
      "name": "<?php echo $post->post_title; ?>"
    }
  }]
}</script>
<?php
  }
  $html = ob_get_contents();
  ob_clean();
  echo $html;
}
add_action('wp_head', 'application_ld_json');
*/

function kama_breadcrumbs($sep = ' » ', $l10n = array(), $args = array()){
  $kb = new Kama_Breadcrumbs;
  echo $kb->get_crumbs( $sep, $l10n, $args );
}
class Kama_Breadcrumbs {
  public $arg;
  // Локализация
  static $l10n = array(
    'home'       => 'Главная',
    'paged'      => 'Страница %d',
    '_404'       => 'Ошибка 404',
    'search'     => 'Результаты поиска по запросу - <b>%s</b>',
    'author'     => 'Архив автора: <b>%s</b>',
    'year'       => 'Архив за <b>%d</b> год',
    'month'      => 'Архив за: <b>%s</b>',
    'day'        => '',
    'attachment' => 'Медиа: %s',
    'tag'        => 'Записи по метке: <b>%s</b>',
    'tax_tag'    => '%1$s из "%2$s" по тегу: <b>%3$s</b>',
    // tax_tag выведет: 'тип_записи из "название_таксы" по тегу: имя_термина'.
    // Если нужны отдельные холдеры, например только имя термина, пишем так: 'записи по тегу: %3$s'
  );
  // Параметры по умолчанию
  static $args = array(
    'on_front_page'   => true,  // выводить крошки на главной странице
    'show_post_title' => true,  // показывать ли название записи в конце (последний элемент). Для записей, страниц, вложений
    'show_term_title' => true,  // показывать ли название элемента таксономии в конце (последний элемент). Для меток, рубрик и других такс
    'title_patt'      => '<span class="kb_title">%s</span>', // шаблон для последнего заголовка. Если включено: show_post_title или show_term_title
    'last_sep'        => true,  // показывать последний разделитель, когда заголовок в конце не отображается
    'markup'          => 'schema.org', // 'markup' - микроразметка. Может быть: 'rdf.data-vocabulary.org', 'schema.org', '' - без микроразметки
                       // или можно указать свой массив разметки:
                       // array( 'wrappatt'=>'<div class="kama_breadcrumbs">%s</div>', 'linkpatt'=>'<a href="%s">%s</a>', 'sep_after'=>'', )
    'priority_tax'    => array('category'), // приоритетные таксономии, нужно когда запись в нескольких таксах
    'priority_terms'  => array(), // 'priority_terms' - приоритетные элементы таксономий, когда запись находится в нескольких элементах одной таксы одновременно.
                    // Например: array( 'category'=>array(45,'term_name'), 'tax_name'=>array(1,2,'name') )
                    // 'category' - такса для которой указываются приор. элементы: 45 - ID термина и 'term_name' - ярлык.
                    // порядок 45 и 'term_name' имеет значение: чем раньше тем важнее. Все указанные термины важнее неуказанных...
    'nofollow' => false, // добавлять rel=nofollow к ссылкам?
    // служебные
    'sep'             => '',
    'linkpatt'        => '',
    'pg_end'          => '',
  );
  function get_crumbs( $sep, $l10n, $args ){
    global $post, $wp_query, $wp_post_types;
    self::$args['sep'] = $sep;
    // Фильтрует дефолты и сливает
    $loc = (object) array_merge( apply_filters('kama_breadcrumbs_default_loc', self::$l10n ), $l10n );
    $arg = (object) array_merge( apply_filters('kama_breadcrumbs_default_args', self::$args ), $args );
    $arg->sep = '<span class="kb_sep">'. $arg->sep .'</span>'; // дополним
    // упростим
    $sep = & $arg->sep;
    $this->arg = & $arg;
    // микроразметка ---
    if(1){
      $mark = & $arg->markup;
      // Разметка по умолчанию
      if( ! $mark ) $mark = array(
        'wrappatt'  => '<div class="kama_breadcrumbs">%s</div>',
        'linkpatt'  => '<a href="%s">%s</a>',
        'sep_after' => '',
      );
      // rdf
      elseif( $mark === 'rdf.data-vocabulary.org' ) $mark = array(
        'wrappatt'   => '<div class="kama_breadcrumbs" prefix="v: http://rdf.data-vocabulary.org/#">%s</div>',
        'linkpatt'   => '<span typeof="v:Breadcrumb"><a href="%s" rel="v:url" property="v:title">%s</a>',
        'sep_after'  => '</span>', // закрываем span после разделителя!
      );
      // schema.org
      elseif( $mark === 'schema.org' ) $mark = array(
        'wrappatt'   => '<div class="kama_breadcrumbs" itemscope itemtype="http://schema.org/BreadcrumbList">%s</div>',
        'linkpatt'   => '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem"><a href="%s" itemprop="item"><span itemprop="name">%s</span></a></span>',
        'sep_after'  => '',
      );
      elseif( ! is_array($mark) )
        die( __CLASS__ .': "markup" parameter must be array...');
      $wrappatt  = $mark['wrappatt'];
      $arg->linkpatt  = $arg->nofollow ? str_replace('<a ','<a rel="nofollow"', $mark['linkpatt']) : $mark['linkpatt'];
      $arg->sep      .= $mark['sep_after']."\n";
    }
    $linkpatt = $arg->linkpatt; // упростим
    $q_obj = get_queried_object();
    // может это архив пустой таксы?
    $ptype = null;
    if( empty($post) ){
      if( isset($q_obj->taxonomy) )
        $ptype = & $wp_post_types[ get_taxonomy($q_obj->taxonomy)->object_type[0] ];
    }
    else $ptype = & $wp_post_types[ $post->post_type ];
    // paged
    $arg->pg_end = '';
    if( ($paged_num = get_query_var('paged')) || ($paged_num = get_query_var('page')) )
      $arg->pg_end = $sep . sprintf( $loc->paged, (int) $paged_num );
    $pg_end = $arg->pg_end; // упростим
    // ну, с богом...
    $out = '';
    if( is_front_page() ){
      return $arg->on_front_page ? sprintf( $wrappatt, ( $paged_num ? sprintf($linkpatt, get_home_url(), $loc->home) . $pg_end : $loc->home ) ) : '';
    }
    // страница записей, когда для главной установлена отдельная страница.
    elseif( is_home() ) {
      $out = $paged_num ? ( sprintf( $linkpatt, get_permalink($q_obj), esc_html($q_obj->post_title) ) . $pg_end ) : esc_html($q_obj->post_title);
    }
    elseif( is_404() ){
      $out = $loc->_404;
    }
    elseif( is_search() ){
      $out = sprintf( $loc->search, esc_html( $GLOBALS['s'] ) );
    }
    elseif( is_author() ){
      $tit = sprintf( $loc->author, esc_html($q_obj->display_name) );
      $out = ( $paged_num ? sprintf( $linkpatt, get_author_posts_url( $q_obj->ID, $q_obj->user_nicename ) . $pg_end, $tit ) : $tit );
    }
    elseif( is_year() || is_month() || is_day() ){
      $y_url  = get_year_link( $year = get_the_time('Y') );
      if( is_year() ){
        $tit = sprintf( $loc->year, $year );
        $out = ( $paged_num ? sprintf($linkpatt, $y_url, $tit) . $pg_end : $tit );
      }
      // month day
      else {
        $y_link = sprintf( $linkpatt, $y_url, $year);
        $m_url  = get_month_link( $year, get_the_time('m') );
        if( is_month() ){
          $tit = sprintf( $loc->month, get_the_time('F') );
          $out = $y_link . $sep . ( $paged_num ? sprintf( $linkpatt, $m_url, $tit ) . $pg_end : $tit );
        }
        elseif( is_day() ){
          $m_link = sprintf( $linkpatt, $m_url, get_the_time('F'));
          $out = $y_link . $sep . $m_link . $sep . get_the_time('l');
        }
      }
    }
    // Древовидные записи
    elseif( is_singular() && $ptype->hierarchical ){
      $out = $this->_add_title( $this->_page_crumbs($post), $post );
    }
    // Таксы, плоские записи и вложения
    else {
      $term = $q_obj; // таксономии
      // определяем термин для записей (включая вложения attachments)
      if( is_singular() ){
        // изменим $post, чтобы определить термин родителя вложения
        if( is_attachment() && $post->post_parent ){
          $save_post = $post; // сохраним
          $post = get_post($post->post_parent);
        }
        // учитывает если вложения прикрепляются к таксам древовидным - все бывает :)
        $taxonomies = get_object_taxonomies( $post->post_type );
        // оставим только древовидные и публичные, мало ли...
        $taxonomies = array_intersect( $taxonomies, get_taxonomies( array('hierarchical' => true, 'public' => true) ) );
        if( $taxonomies ){
          // сортируем по приоритету
          if( ! empty($arg->priority_tax) ){
            usort( $taxonomies, function($a,$b)use($arg){
              $a_index = array_search($a, $arg->priority_tax);
              if( $a_index === false ) $a_index = 9999999;
              $b_index = array_search($b, $arg->priority_tax);
              if( $b_index === false ) $b_index = 9999999;
              return ( $b_index === $a_index ) ? 0 : ( $b_index < $a_index ? 1 : -1 ); // меньше индекс - выше
            } );
          }
          // пробуем получить термины, в порядке приоритета такс
          foreach( $taxonomies as $taxname ){
            if( $terms = get_the_terms( $post->ID, $taxname ) ){
              // проверим приоритетные термины для таксы
              $prior_terms = & $arg->priority_terms[ $taxname ];
              if( $prior_terms && count($terms) > 2 ){
                foreach( (array) $prior_terms as $term_id ){
                  $filter_field = is_numeric($term_id) ? 'term_id' : 'slug';
                  $_terms = wp_list_filter( $terms, array($filter_field=>$term_id) );
                  if( $_terms ){
                    $term = array_shift( $_terms );
                    break;
                  }
                }
              }
              else
                $term = array_shift( $terms );
              break;
            }
          }
        }
        if( isset($save_post) ) $post = $save_post; // вернем обратно (для вложений)
      }
      // вывод
      // все виды записей с терминами или термины
      if( $term && isset($term->term_id) ){
        $term = apply_filters('kama_breadcrumbs_term', $term );
        // attachment
        if( is_attachment() ){
          if( ! $post->post_parent )
            $out = sprintf( $loc->attachment, esc_html($post->post_title) );
          else {
            if( ! $out = apply_filters('attachment_tax_crumbs', '', $term, $this ) ){
              $_crumbs    = $this->_tax_crumbs( $term, 'self' );
              $parent_tit = sprintf( $linkpatt, get_permalink($post->post_parent), get_the_title($post->post_parent) );
              $_out = implode( $sep, array($_crumbs, $parent_tit) );
              $out = $this->_add_title( $_out, $post );
            }
          }
        }
        // single
        elseif( is_single() ){
          if( ! $out = apply_filters('post_tax_crumbs', '', $term, $this ) ){
            $_crumbs = $this->_tax_crumbs( $term, 'self' );
            $out = $this->_add_title( $_crumbs, $post );
          }
        }
        // не древовидная такса (метки)
        elseif( ! is_taxonomy_hierarchical($term->taxonomy) ){
          // метка
          if( is_tag() )
            $out = $this->_add_title('', $term, sprintf( $loc->tag, esc_html($term->name) ) );
          // такса
          elseif( is_tax() ){
            $post_label = $ptype->labels->name;
            $tax_label = $GLOBALS['wp_taxonomies'][ $term->taxonomy ]->labels->name;
            $out = $this->_add_title('', $term, sprintf( $loc->tax_tag, $post_label, $tax_label, esc_html($term->name) ) );
          }
        }
        // древовидная такса (рибрики)
        else {
          if( ! $out = apply_filters('term_tax_crumbs', '', $term, $this ) ){
            $_crumbs = $this->_tax_crumbs( $term, 'parent' );
            $out = $this->_add_title( $_crumbs, $term, esc_html($term->name) );                   
          }
        }
      }
      // влоежния от записи без терминов
      elseif( is_attachment() ){
        $parent = get_post($post->post_parent);
        $parent_link = sprintf( $linkpatt, get_permalink($parent), esc_html($parent->post_title) );
        $_out = $parent_link;
        // вложение от записи древовидного типа записи
        if( is_post_type_hierarchical($parent->post_type) ){
          $parent_crumbs = $this->_page_crumbs($parent);
          $_out = implode( $sep, array( $parent_crumbs, $parent_link ) );
        }
        $out = $this->_add_title( $_out, $post );
      }
      // записи без терминов
      elseif( is_singular() ){
        $out = $this->_add_title( '', $post );
      }
    }
    // замена ссылки на архивную страницу для типа записи
    $home_after = apply_filters('kama_breadcrumbs_home_after', '', $linkpatt, $sep, $ptype );
    if( '' === $home_after ){
      // Ссылка на архивную страницу типа записи для: отдельных страниц этого типа; архивов этого типа; таксономий связанных с этим типом.
      if( $ptype && $ptype->has_archive && ! in_array( $ptype->name, array('post','page','attachment') )
        && ( is_post_type_archive() || is_singular() || (is_tax() && in_array($term->taxonomy, $ptype->taxonomies)) )
      ){
        $pt_title = $ptype->labels->name;
        // первая страница архива типа записи
        if( is_post_type_archive() && ! $paged_num )
          $home_after = sprintf( $this->arg->title_patt, $pt_title );
        // singular, paged post_type_archive, tax
        else{
          $home_after = sprintf( $linkpatt, get_post_type_archive_link($ptype->name), $pt_title );
          $home_after .= ( ($paged_num && ! is_tax()) ? $pg_end : $sep ); // пагинация
        }
      }
    }
    $before_out = sprintf( $linkpatt, home_url(), $loc->home ) . ( $home_after ? $sep.$home_after : ($out ? $sep : '') );
    $out = apply_filters('kama_breadcrumbs_pre_out', $out, $sep, $loc, $arg );
    $out = sprintf( $wrappatt, $before_out . $out );
    return apply_filters('kama_breadcrumbs', $out, $sep, $loc, $arg );
  }
  function _page_crumbs( $post ){
    $parent = $post->post_parent;
    $crumbs = array();
    while( $parent ){
      $page = get_post( $parent );
      $crumbs[] = sprintf( $this->arg->linkpatt, get_permalink($page), esc_html($page->post_title) );
      $parent = $page->post_parent;
    }
    return implode( $this->arg->sep, array_reverse($crumbs) );
  }
  function _tax_crumbs( $term, $start_from = 'self' ){
    $termlinks = array();
    $term_id = ($start_from === 'parent') ? $term->parent : $term->term_id;
    while( $term_id ){
      $term       = get_term( $term_id, $term->taxonomy );
      $termlinks[] = sprintf( $this->arg->linkpatt, get_term_link($term), esc_html($term->name) );
      $term_id    = $term->parent;
    }
    if( $termlinks )
      return implode( $this->arg->sep, array_reverse($termlinks) ) /*. $this->arg->sep*/;
    return '';
  }
  // добалвяет заголовок к переданному тексту, с учетом всех опций. Добавляет разделитель в начало, если надо.
  function _add_title( $add_to, $obj, $term_title = '' ){
    $arg = & $this->arg; // упростим...
    $title = $term_title ? $term_title : esc_html($obj->post_title); // $term_title чиститься отдельно, теги моугт быть...
    $show_title = $term_title ? $arg->show_term_title : $arg->show_post_title;
    // пагинация
    if( $arg->pg_end ){
      $link = $term_title ? get_term_link($obj) : get_permalink($obj);
      $add_to .= ($add_to ? $arg->sep : '') . sprintf( $arg->linkpatt, $link, $title ) . $arg->pg_end;
    }
    // дополняем - ставим sep
    elseif( $add_to ){
      if( $show_title )
        $add_to .= $arg->sep . sprintf( $arg->title_patt, $title );
      elseif( $arg->last_sep )
        $add_to .= $arg->sep;
    }
    // sep будет потом...
    elseif( $show_title )
      $add_to = sprintf( $arg->title_patt, $title );
    return $add_to;
  }
}

function kama_pagenavi( $args = array(), $wp_query = null ){
  // параметры по умолчанию
  $default = array(
    'before'          => '',   // Текст до навигации.
    'after'           => '',   // Текст после навигации.
    'echo'            => true, // Возвращать или выводить результат.

    'text_num_page'   => '',           // Текст перед пагинацией.
    // {current} - текущая.
    // {last} - последняя (пр: 'Страница {current} из {last}' получим: "Страница 4 из 60").
    'num_pages'       => 10,           // Сколько ссылок показывать.
    'step_link'       => 10,           // Ссылки с шагом (если 10, то: 1,2,3...10,20,30. Ставим 0, если такие ссылки не нужны.
    'dotright_text'   => '…',          // Промежуточный текст "до".
    'dotright_text2'  => '…',          // Промежуточный текст "после".
    'back_text'       => '« назад',    // Текст "перейти на предыдущую страницу". Ставим 0, если эта ссылка не нужна.
    'next_text'       => 'вперед »',   // Текст "перейти на следующую страницу".  Ставим 0, если эта ссылка не нужна.
    'first_page_text' => '« к началу', // Текст "к первой странице".    Ставим 0, если вместо текста нужно показать номер страницы.
    'last_page_text'  => 'в конец »',  // Текст "к последней странице". Ставим 0, если вместо текста нужно показать номер страницы.
  );

  // Cовместимость с v2.5: kama_pagenavi( $before = '', $after = '', $echo = true, $args = array() )
  if( ($fargs = func_get_args()) && is_string( $fargs[0] ) ){
    $default['before'] = isset($fargs[0]) ? $fargs[0] : '';
    $default['after']  = isset($fargs[1]) ? $fargs[1] : '';
    $default['echo']   = isset($fargs[2]) ? $fargs[2] : true;
    $args              = isset($fargs[3]) ? $fargs[3] : array();
    $wp_query = $GLOBALS['wp_query']; // после определения $default!
  }

  if( ! $wp_query ){
    wp_reset_query();
    global $wp_query;
  }

  if( ! $args ) $args = array();
  if( $args instanceof WP_Query ){
    $wp_query = $args;
    $args     = array();
  }

  $default = apply_filters( 'kama_pagenavi_args', $default ); // чтобы можно было установить свои значения по умолчанию

  $rg = (object) array_merge( $default, $args );

  //$posts_per_page = (int) $wp_query->get('posts_per_page');
  $paged          = (int) $wp_query->get('paged');
  $max_page       = $wp_query->max_num_pages;

  // проверка на надобность в навигации
  if( $max_page <= 1 )
    return false;

  if( empty( $paged ) || $paged == 0 )
    $paged = 1;

  $pages_to_show = intval( $rg->num_pages );
  $pages_to_show_minus_1 = $pages_to_show-1;

  $half_page_start = floor( $pages_to_show_minus_1/2 ); // сколько ссылок до текущей страницы
  $half_page_end   = ceil(  $pages_to_show_minus_1/2 ); // сколько ссылок после текущей страницы

  $start_page = $paged - $half_page_start; // первая страница
  $end_page   = $paged + $half_page_end;   // последняя страница (условно)

  if( $start_page <= 0 )
    $start_page = 1;
  if( ($end_page - $start_page) != $pages_to_show_minus_1 )
    $end_page = $start_page + $pages_to_show_minus_1;
  if( $end_page > $max_page ) {
    $start_page = $max_page - $pages_to_show_minus_1;
    $end_page = (int) $max_page;
  }

  if( $start_page <= 0 )
    $start_page = 1;

  // создаем базу чтобы вызвать get_pagenum_link один раз
  $link_base = str_replace( 99999999, '___', get_pagenum_link( 99999999 ) );
  $first_url = get_pagenum_link( 1 );
  if( false === strpos( $first_url, '?') )
    $first_url = user_trailingslashit( $first_url );

  // собираем елементы
  $els = array();

  if( $rg->text_num_page ){
    $rg->text_num_page = preg_replace( '!{current}|{last}!', '%s', $rg->text_num_page );
    $els['pages'] = sprintf( '<span class="pages">'. $rg->text_num_page .'</span>', $paged, $max_page );
  }
  // назад
  if ( $rg->back_text && $paged != 1 )
    $els['prev'] = '<a class="prev" href="'. ( ($paged-1)==1 ? $first_url : str_replace( '___', ($paged-1), $link_base ) ) .'">'. $rg->back_text .'</a>';
  // в начало
  if ( $start_page >= 2 && $pages_to_show < $max_page ) {
    $els['first'] = '<a class="first" href="'. $first_url .'">'. ( $rg->first_page_text ?: 1 ) .'</a>';
    if( $rg->dotright_text && $start_page != 2 )
      $els[] = '<span class="extend">'. $rg->dotright_text .'</span>';
  }
  // пагинация
  for( $i = $start_page; $i <= $end_page; $i++ ) {
    if( $i == $paged )
      $els['current'] = '<span class="current">'. $i .'</span>';
    elseif( $i == 1 )
      $els[] = '<a href="'. $first_url .'">1</a>';
    else
      $els[] = '<a href="'. str_replace( '___', $i, $link_base ) .'">'. $i .'</a>';
  }

  // ссылки с шагом
  $dd = 0;
  if ( $rg->step_link && $end_page < $max_page ){
    for( $i = $end_page + 1; $i <= $max_page; $i++ ){
      if( $i % $rg->step_link == 0 && $i !== $rg->num_pages ) {
        if ( ++$dd == 1 )
          $els[] = '<span class="extend">'. $rg->dotright_text2 .'</span>';
        $els[] = '<a href="'. str_replace( '___', $i, $link_base ) .'">'. $i .'</a>';
      }
    }
  }
  // в конец
  if ( $end_page < $max_page ) {
    if( $rg->dotright_text && $end_page != ($max_page-1) )
      $els[] = '<span class="extend">'. $rg->dotright_text2 .'</span>';
    $els['last'] = '<a class="last" href="'. str_replace( '___', $max_page, $link_base ) .'">'. ( $rg->last_page_text ?: $max_page ) .'</a>';
  }
  // вперед
  if ( $rg->next_text && $paged != $end_page )
    $els['next'] = '<a class="next" href="'. str_replace( '___', ($paged+1), $link_base ) .'">'. $rg->next_text .'</a>';

  $els = apply_filters( 'kama_pagenavi_elements', $els );

  $out = $rg->before . '<div class="wp-pagenavi">'. implode( ' ', $els ) .'</div>'. $rg->after;

  $out = apply_filters( 'kama_pagenavi', $out );

  if( $rg->echo ) echo $out;
  else return $out;
}

/***** Category SEO */
add_action("category_edit_form_fields", 'seocattag_category_meta');
function seocattag_category_meta($term){
?>
  <tr class="form-field">
    <th scope="row" valign="top"><label>Заголовок (title)</label></th>
    <td>
      <input type="text" name="seocattag[title]" value="<?php echo esc_attr(get_term_meta($term->term_id, 'title', 1)); ?>"><br />
    </td>
  </tr>
  <tr class="form-field">
    <th scope="row" valign="top"><label>Краткое описание (description)</label></th>
    <td>
      <input type="text" name="seocattag[description]" value="<?php echo esc_attr(get_term_meta($term->term_id, 'description', 1)); ?>"><br />
    </td>
  </tr>
  <tr class="form-field">
    <th scope="row" valign="top"><label>Ключевые слова (keywords)</label></th>
    <td>
      <input type="text" name="seocattag[keywords]" value="<?php echo esc_attr(get_term_meta($term->term_id, 'keywords', 1)); ?>"><br />
    </td>
  </tr>
<?php
}

function seocattag_save_meta($term_id){
  if(!isset($_POST['seocattag']))
    return;
  $seocattag = array_map('trim', $_POST['seocattag']);
  foreach($seocattag as $key => $value){
    if(empty($value)){
      delete_term_meta($term_id, $key);
      continue;
    }
    update_term_meta($term_id, $key, $value);
  }
  return $term_id;
}
add_action("create_category", 'seocattag_save_meta');
add_action("edited_category", 'seocattag_save_meta');


function seocattag_filter_single_cat_title($term_name){
    $terms = get_category( get_query_var('cat'));
    $cat_id = $terms->cat_ID;
    $term_name = get_term_meta($cat_id, 'title', true);
    return $term_name; 
}
add_filter('single_cat_title', 'seocattag_filter_single_cat_title', 10, 1);

function seocattag_single_cat_title($term_name){
  if(empty($term_name)){
    $terms = get_category(get_query_var('cat'));
    $cat_id = $terms->cat_ID;
    $term_name = get_cat_name($cat_id);
  }
  return $term_name;
}
add_filter('single_cat_title', 'seocattag_single_cat_title', 10, 1);

function seocattag_cat_description($description){
  if(is_category()){
    $desc = get_category(get_query_var('cat'));
    $category_id = $desc->cat_ID;
    $description = get_term_meta($category_id, 'description', true);
    if(!empty($description)){
      $meta = '<meta name="description"  content="'.$description.'" />'."\n";
    } else {        
      $description = wp_filter_nohtml_kses(substr(category_description(), 0, 280));
      $meta = '<meta name="description" content="'.$description.'" />'."\n";      
    }
    echo $meta;
  }
}
add_action('wp_head', 'seocattag_cat_description', 1, 1);

function seocattag_cat_keywords($keywords){
  if(is_category()){
    $terms = get_category(get_query_var('cat'));
    $cat_id = $terms->cat_ID;
    $keywords = get_term_meta($cat_id, 'keywords', true);
    echo '<meta name="keywords" content="'.$keywords.'">'."\n";
  }
}
add_action('wp_head', 'seocattag_cat_keywords', 1, 1);
/* End category SEO*/

add_action('wp_head', 'translate_strings_js');
function translate_strings_js(){
?>
  <script type='text/javascript'>
  /* <![CDATA[ */
  var translate_text = {
    'price_request__title': '<?php _e('Запит ціни', 'walldi'); ?>',
    'price_request__name': '<?php _e("І\'мя", 'walldi'); ?>',
    'price_request__phone': '<?php _e('Телефон/Viber', 'walldi'); ?>',
    'price_request__city': '<?php _e('Місто', 'walldi'); ?>',
    'price_request__availability': '<?php _e('Наявність проекту', 'walldi'); ?>',
    'price_request__installation': '<?php _e('Потрібен монтаж', 'walldi'); ?>',
    'price_request__message': '<?php _e('Повідомлення', 'walldi'); ?>',
    'price_request__send': '<?php _e('Відправити', 'walldi'); ?>',
    'callback__title' : '<?php _e("Зв\'яжіться з нами", 'walldi'); ?>',
    'callback__name' : '<?php _e("І\'мя", 'walldi'); ?>',
    'callback__phone' : '<?php _e('Телефон', 'walldi'); ?>',
    'callback__message' : '<?php _e('Повідомлення', 'walldi'); ?>',
    'callback__send' : '<?php _e('Відправити', 'walldi'); ?>',
  };
  /* ]]> */
  </script>
<?php
}

if(function_exists('acf_add_options_page')){
  $parent = acf_add_options_page(array(
    'page_title' => 'Theme General Settings',
    'menu_title' => 'Theme Settings',
    'redirect'   => 'Theme Settings'
  ));
  
  $languages = array_values(pll_languages_list());

  foreach($languages as $lang){
    acf_add_options_sub_page(array(
      'page_title' => 'Options (' . strtoupper($lang) . ')',
      'menu_title' => 'Options (' . strtoupper($lang) . ')',
      'menu_slug'  => "options-${lang}",
      'post_id'    => $lang,
      'parent'     => $parent['menu_slug']
    ));
  }
}
?>