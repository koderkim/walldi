(function(){
	if(!Element.prototype.matches){
		Element.prototype.matches = Element.prototype.matchesSeleector || Element.prototype.webkitMatchesSelector || Element.prototype.mozMatchesSelector || Element.prototype.msMatchesSelctor
	}
})();

(function(){
	if(!Element.prototype.closest){
		Element.prototype.closest = function(css){
			var node = this;
			while(node){
				if(node.matches(css)) return node;
				else node = node.parentElement;
			}
			return null;
		}
	}
})();

/*function setCookie(name, value, options){
	options = options || {};
	var expires = options.expires;
	if(typeof expires == "number" && expires){
		var d = new Date();
		d.setTime(d.getTime() + expires * 1000);
		expires = options.expires = d;
	}
	if(expires && expires.toUTCString){
		options.expires = expires.toUTCString();
	}
	value = encodeURIComponent(value);
	var updatedCookie = name + "=" + value;
	for(var propName in options){
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if(propValue !== true){
			updatedCookie += "=" + propValue;
		}
	}
	document.cookie = updatedCookie;
}

function getCookie(name){
	var matches = document.cookie.match(new RegExp("(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

function http_build_query(formdata, numeric_prefix, arg_separator){
	var key, use_val, use_key, i = 0, tmp_arr = [];
	if(!arg_separator){
		arg_separator = '&';
	}
	for(key in formdata){
		use_key = escape(key);
		use_val = escape((formdata[key].toString()));
		use_val = use_val.replace(/%20/g, '+');

		if(numeric_prefix && !isNaN(key)){
			use_key = numeric_prefix + i;
		}
		tmp_arr[i] = use_key + '=' + use_val;
		i++;
	}
	return tmp_arr.join(arg_separator);
}

function ajaxSendLike(data, callback){
	var http = new XMLHttpRequest();
	var url = window.wpData.ajaxUrl;
	var params = http_build_query(data, '', '&');

	http.open('POST', url, true);
	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
	
	http.onreadystatechange = function(){
		if(http.readyState == 4 && http.status == 200) {
		    if(typeof callback == 'function'){
		    	callback(http.responseText);
		    } else {
		    	console.log(http.responseText);
		    }
		}
	}
	
	http.send(params);
}*/

window.addEventListener('DOMContentLoaded', function(){
	var catalogmenu = document.querySelector('#catalogmenu');
	var menumin = document.querySelector('#menumin');
	if(catalogmenu && menumin){
		menumin.addEventListener('click', function(){
			catalogmenu.classList.toggle('open');
		});
	}

	var tabItems = document.querySelectorAll('.tab__item-title');
	if(tabItems){
		for(var i=0; i<tabItems.length; i++){
			tabItems[i].addEventListener('click', function(){
				this.closest('.tab__item').classList.toggle('open');
			});
		}
	}

	//var index = Array.from(this.parentElement.parentElement.parentElement.children).indexOf(this.parentNode.parentNode);
});

/*window.addEventListener('scroll', function(){
	var scrollTop = window.pageYOffset || document.documentElement.scrollTop;
});*/