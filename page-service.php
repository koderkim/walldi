<?php
// Template name: Services
?>
<?php get_header(); ?>
	<main>
	<?php if(have_posts()): ?>
		<?php while(have_posts()): the_post(); ?>
		<div class="container">
			<h1 class="page-title"><?php the_title(); ?></h1>
		</div>
		
		<section>
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>

		<?php if($services_list = get_field('services_list')): ?>
		<section>
			<div class="container">
				<?php foreach($services_list as $item): ?>
				<div class="services__item" style="background-image:url(<?php echo wp_get_attachment_url($item['image']); ?>)">
					<div class="row">
						<div class="col-md-8">
							<div class="services__item-box">
								<div class="services__item-title"><?php echo $item['title']; ?></div>
								<div class="services__item-content"><?php echo $item['content']; ?></div>
							</div>
						</div>
						<div class="col-md-4">
							<img src="<?php echo wp_get_attachment_url($item['image']); ?>" alt="<?php echo $item['title']; ?>" class="img-responsive">
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
		</section>
		<?php endif; ?>
		<?php endwhile; ?>
	<?php endif; ?>
	</main>
<?php get_footer(); ?>