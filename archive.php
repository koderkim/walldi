<?php get_header(); ?>
		<div class="container">
			<h1 class="page-title"><?php echo single_cat_title(); ?></h1>
		</div>

		<?php $category = get_queried_object(); ?>
		<div class="category_list">
			<?php if(have_posts()): ?>
				<?php while(have_posts()): the_post(); ?>
			<div class="category_list__item">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="category_list__item-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
							<div class="category_list__item-description">
								<?php the_content(); ?>
								<?php /* ?><strong><?php _e('Цена', 'walldi'); ?> <?php echo get_field('good_price'); ?></strong><?php */ ?>
							</div>
							<div class="category_list__item-link"><a href="<?php the_permalink(); ?>" class="btn btn__link"><?php _e('more', 'walldi'); ?></a></div>
						</div>
						<div class="col-md-8"<?php echo has_post_thumbnail() ? ' style="background-image:url('.get_the_post_thumbnail_url(get_the_ID(), 'full').')"' : ''; ?>>
							<?php if(has_post_thumbnail()): ?>
							<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" alt="" class="img-responsive">
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
				<?php endwhile; ?>
			<?php else: ?>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
						<?php echo category_description($category->term_id); ?>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
		<?php if($additional_description = get_field('additional_description', 'category_'.$category->term_id)): ?>
		<div class="category__additional-text">
			<div class="container">
				<div class="category__additional-text_wrap short">
					<?php echo $additional_description; ?>
				</div>
				<div class="category__additional-text_wrap-btn" data-btn-text="<?php _e('Скрыть', 'walldi'); ?>"><?php _e('Подробнее', 'walldi'); ?></div>
			</div>
		</div>
		<script>
			window.addEventListener('DOMContentLoaded', function(){
				var btn = document.querySelector('.category__additional-text_wrap-btn');
				if(btn){
					var wrap = document.querySelector('.category__additional-text_wrap');
					if(wrap){
						btn.addEventListener('click', function(){
							wrap.classList.toggle('short');
							var btn_text = btn.innerHTML;
							btn.innerHTML = btn.getAttribute('data-btn-text');
							btn.setAttribute('data-btn-text', btn_text);
						});
					}
				}
			});
		</script>
		<?php endif; ?>
<?php get_footer(); ?>