<?php get_header(); ?>
<?php
	$good_photos = get_field('good_photos');
	$photos = array();
	if($good_photos):
		foreach($good_photos as $item):
			$photos[] = array(
				'thumb' => wp_get_attachment_image_url($item['photo']),
				'photo' => wp_get_attachment_image_url($item['photo'], 'full'),
			);
		endforeach;
	endif;
?>
		<section>
			<div class="container">
				<?php echo kama_breadcrumbs(' > '); ?>
			</div>
		</section>
		<?php if(have_posts()): ?>
			<?php while(have_posts()): the_post(); ?>
		<section>
			<div class="container">
				<div class="single_title"><?php the_title(); ?></div>
			</div>
			<?php if($photos): ?>
			<div class="gallery__slider">
				<div class="slider__outer">
					<div class="slider__wrapper">
						<?php foreach($photos as $item): ?>
						<div class="slider__item">
							<img src="<?php echo $item['photo']; ?>" alt="" class="img-responsive">
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<a class="slider__control slider__control_left" role="button"></a>
				<a class="slider__control slider__control_right" role="button"></a>
			</div>
			<?php endif; ?>
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<div class="gallery">
							<?php if($photos): ?>
							<div class="gallery__main"><img src="<?php echo $photos[0]['photo']; ?>" alt=""></div>
							<div class="gallery__preview">
								<?php foreach($photos as $index => $item): ?>
								<div class="gallery__preview-item<?php echo $index ==0 ? ' active' : ''; ?>" data-src="<?php echo $item['photo']; ?>">
									<img src="<?php echo $item['thumb']; ?>" alt="">
								</div>
								<?php endforeach; ?>
							</div>
							<?php endif; ?>
						</div>
					</div>
					<div class="col-md-4">
						<?php //$open = ' open'; ?>
						<?php if($specification = get_field('good_specification')): ?>
						<div class="tab__item<?php echo $open; ?>">
							<div class="tab__item-title"><?php _e('Specification', 'walldi'); ?></div>
							<div class="tab__item-description">
								<?php echo $specification; ?>
							</div>
						</div>
							<?php //$open = ' open'; ?>
						<?php endif; ?>
						<?php if($description = get_field('good_description')): ?>
						<div class="tab__item<?php echo $open; ?>">
							<div class="tab__item-title"><?php _e('Description', 'walldi'); ?></div>
							<div class="tab__item-description">
								<?php echo $description; ?>
							</div>
						</div>
							<?php //$open = ''; ?>
						<?php endif; ?>
						<?php if($good_catalog = get_field('good_catalog')): ?>
						<div class="good-price">
							<?php foreach($good_catalog as $item): ?>
							<div>
								<svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M24.375 14.625V13H19.5V21.125H21.125V17.875H23.5625V16.25H21.125V14.625H24.375Z" fill="black"/><path d="M15.4375 21.125H12.1875V13H15.4375C16.0838 13.0006 16.7034 13.2577 17.1604 13.7146C17.6173 14.1716 17.8744 14.7912 17.875 15.4375V18.6875C17.8744 19.3338 17.6173 19.9534 17.1604 20.4104C16.7034 20.8673 16.0838 21.1244 15.4375 21.125ZM13.8125 19.5H15.4375C15.6529 19.4998 15.8595 19.4141 16.0118 19.2618C16.1641 19.1095 16.2498 18.9029 16.25 18.6875V15.4375C16.2498 15.2221 16.1641 15.0155 16.0118 14.8632C15.8595 14.7109 15.6529 14.6252 15.4375 14.625H13.8125V19.5Z" fill="black"/><path d="M8.9375 13H4.875V21.125H6.5V18.6875H8.9375C9.36828 18.6869 9.78123 18.5154 10.0858 18.2108C10.3904 17.9062 10.5619 17.4933 10.5625 17.0625V14.625C10.5621 14.1942 10.3907 13.7811 10.0861 13.4764C9.78142 13.1718 9.36834 13.0004 8.9375 13ZM6.5 17.0625V14.625H8.9375L8.93831 17.0625H6.5Z" fill="black"/><path d="M17.875 11.3753V8.12528C17.8779 8.01849 17.8576 7.91236 17.8155 7.81418C17.7734 7.71599 17.7106 7.62809 17.6313 7.55653L11.9437 1.86903C11.8722 1.78968 11.7843 1.7268 11.6861 1.68472C11.5879 1.64263 11.4818 1.62235 11.375 1.62528H3.25C2.81942 1.62656 2.40684 1.79818 2.10237 2.10265C1.7979 2.40712 1.62629 2.81969 1.625 3.25028V22.7503C1.625 23.1813 1.7962 23.5946 2.10095 23.8993C2.4057 24.2041 2.81902 24.3753 3.25 24.3753H16.25V22.7503H3.25V3.25028H9.75V8.12528C9.75129 8.55586 9.9229 8.96844 10.2274 9.2729C10.5318 9.57737 10.9444 9.74899 11.375 9.75028H16.25V11.3753H17.875ZM11.375 8.12528V3.57528L15.925 8.12528H11.375Z" fill="black"/></svg>
								<a href="<?php echo addhttp($item['link_url']); ?>" target="_blank"><?php echo $item['link_text']; ?></a>
							</div>
							<?php endforeach; ?>
						</div>
						<?php endif; ?>
						<div class="good-info">
							<strong><?php _e('Цена', 'walldi'); ?>: <?php echo get_field('good_price'); ?></strong>
						</div>
						<div>
							<a href="#" class="btn price-request" onclick="price_request.open();return false"><?php _e('Price request', 'walldi'); ?></a>
							<a href="#callback" class="btn"><?php _e('Contact us', 'walldi'); ?></a>
						</div>
					</div>
				</div>
			</div>
		</section>
			<?php endwhile; ?>
		<?php endif; ?>

		<?php
			$args = array(
				'post__not_in' => array(get_the_ID())
			);
			$query = new WP_Query($args);
			if($query->have_posts()):
		?>
		<section id="other-goods">
			<div class="container">
				<div class="section-title"><?php _e('Другие товары', 'walldi'); ?></div>
				<div class="slider">
					<div class="slider__outer">
						<div class="slider__wrapper">
							<?php while($query->have_posts()): $query->the_post(); ?>
							<div class="slider__item">
								<div class="slider__item-photo"<?php /* ?> style="background-image:url(<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>)"<?php */ ?>>
									<?php if(has_post_thumbnail()): ?>
									<a href="<?php the_permalink(); ?>"><img decoding="async" src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'medium'); ?>" alt="<?php the_title(); ?>"></a>
									<?php endif; ?>
								</div>
								<div class="slider__item-title">
									<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
								</div>
							</div>
							<?php endwhile; ?>
						</div>
					</div>
					<a class="slider__control slider__control_left slider__control_show" role="button"></a>
					<a class="slider__control slider__control_right slider__control_show" role="button"></a>
				</div>
			</div>
		</section>
		<?php endif; ?>
		<?php wp_reset_postdata(); ?>
		<div class="overlay" id="price_request">
			<div class="overlay-box">
				<div class="overlay-close" onclick="price_request.close();return false">&times;</div>
				<div class="overlay-title"><?php _e('Запрос цены', 'walldi'); ?></div>
				<?php echo do_shortcode('[contact-form-7 id="94" title="Запрос цены"]'); ?>
			</div>
		</div>
<script src="<?php echo get_template_directory_uri(); ?>/js/slider.js"></script>
<script>
	multiItemSlider('.slider');
	window.addEventListener('DOMContentLoaded', function(){
		var galleryThumb = document.querySelectorAll('.gallery__preview-item');
		if(galleryThumb){
			for(var i=0; i<galleryThumb.length; i++){
				galleryThumb[i].addEventListener('click', function(){
					galleryThumb.forEach(function(item){
						item.classList.remove('active');
					});
					this.classList.add('active');
					var src = this.getAttribute('data-src');
					this.parentNode.parentNode.querySelector('.gallery__main img').setAttribute('src', src);
				});
			}
		}
	});
	
	var touchStartX = 0, 
		touchEndX = 0,
		gallery = document.querySelectorAll('.gallery__slider, .slider');
	if(gallery){
		for(var i=0; i<gallery.length; i++){
			gallery[i].addEventListener('touchstart', function(e){
				touchStartX = e.touches[0].clientX;
			});
			gallery[i].addEventListener('touchend', function(e){
				touchEndX = e.changedTouches[0].clientX;
				if(touchStartX < touchEndX){
					this.querySelector('.slider__control_left').click();
				} else if(touchStartX > touchEndX){
					this.querySelector('.slider__control_right').click();
				}
			});
		}
	}
	multiItemSlider('.gallery__slider');
</script>
<?php get_footer(); ?>