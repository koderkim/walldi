	<?php if(!(is_front_page()) || is_single()): ?>
		<section class="callback" id="callback">
			<div class="container">
				<div class="callback-title"><?php _e('Свяжитесь с нами', 'walldi'); ?></div>
				<?php /* ?><form action="">
					<div class="row jcc">
						<div class="col-md-5">
							<div class="form-control">
								<input type="text" placeholder="<?php _e('Имя', 'walldi'); ?>">
							</div>
							<div class="form-control">
								<input type="tel" placeholder="<?php _e('Телефон', 'walldi'); ?>">
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-control">
								<textarea placeholder="<?php _e('Сообщение', 'walldi'); ?>"></textarea>
							</div>
						</div>
						<div class="col-md-10">
							<div class="form-control dfx jcc">
								<input type="submit" class="btn btn__link" value="<?php _e('Отправить', 'walldi'); ?>">
							</div>
						</div>
					</div>
				</form><?php */ ?>
				<?php echo do_shortcode('[contact-form-7 id="98" title="Свяжитесь с нами"]'); ?>
			</div>
		</section>
	<?php endif; ?>
	</main>
	<footer>
		<div id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="footermenu">
							<?php if(function_exists('dynamic_sidebar')) dynamic_sidebar('footer1'); ?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="footermenu">
							<?php if(function_exists('dynamic_sidebar')) dynamic_sidebar('footer2'); ?>
						</div>
					</div>
					<div class="col-md-4">
						<div class="footermenu">
							<div class="footermenu__title"><?php _e('Contacts', 'walldi'); ?></div>
							<div class="footermenu__list">
								<ul>
									<?php
										if($phones = get_theme_mod('walldi_phone')):
											$phones = explode("\n", $phones);
											foreach($phones as $phone):
									?>
									<li><a href="tel:<?php echo preg_replace('/[^0-9+]/', '', $phone); ?>"><?php echo $phone; ?></a></li>
									<?php endforeach; ?>
									<?php endif; ?>
									<?php if($email = get_theme_mod('walldi_email')): ?>
									<li><a href="mailto:<?php echo $email; ?>"><strong><?php echo $email; ?></strong></a></li>
									<?php endif; ?>
									<div class="social">
										<?php if($instagram = get_theme_mod('walldi_social_in')): ?>
										<a href="<?php echo addhttp($instagram); ?>" target="_blank" rel="nofollow"><svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M9.0001 4.59349C6.56163 4.59349 4.59366 6.56145 4.59366 8.99993C4.59366 11.4384 6.56163 13.4064 9.0001 13.4064C11.4386 13.4064 13.4065 11.4384 13.4065 8.99993C13.4065 6.56145 11.4386 4.59349 9.0001 4.59349ZM9.0001 11.8638C7.42315 11.8638 6.13623 10.5769 6.13623 8.99993C6.13623 7.42298 7.42315 6.13606 9.0001 6.13606C10.5771 6.13606 11.864 7.42298 11.864 8.99993C11.864 10.5769 10.5771 11.8638 9.0001 11.8638ZM13.587 3.38606C13.0177 3.38606 12.5579 3.84583 12.5579 4.41517C12.5579 4.9845 13.0177 5.44427 13.587 5.44427C14.1564 5.44427 14.6161 4.98665 14.6161 4.41517C14.6163 4.27997 14.5898 4.14608 14.5381 4.02114C14.4865 3.89621 14.4107 3.7827 14.3151 3.6871C14.2195 3.59151 14.106 3.51571 13.981 3.46405C13.8561 3.4124 13.7222 3.38589 13.587 3.38606ZM17.5896 8.99993C17.5896 7.81399 17.6003 6.6388 17.5337 5.45501C17.4671 4.08001 17.1534 2.8597 16.148 1.85423C15.1403 0.846611 13.9222 0.535087 12.5472 0.468486C11.3612 0.401884 10.186 0.412626 9.00225 0.412626C7.81631 0.412626 6.64112 0.401884 5.45733 0.468486C4.08233 0.535087 2.86202 0.848759 1.85655 1.85423C0.84893 2.86185 0.537407 4.08001 0.470805 5.45501C0.404203 6.64095 0.414946 7.81614 0.414946 8.99993C0.414946 10.1837 0.404203 11.3611 0.470805 12.5449C0.537407 13.9199 0.851079 15.1402 1.85655 16.1456C2.86416 17.1533 4.08233 17.4648 5.45733 17.5314C6.64327 17.598 7.81846 17.5872 9.00225 17.5872C10.1882 17.5872 11.3634 17.598 12.5472 17.5314C13.9222 17.4648 15.1425 17.1511 16.148 16.1456C17.1556 15.138 17.4671 13.9199 17.5337 12.5449C17.6024 11.3611 17.5896 10.1859 17.5896 8.99993ZM15.6989 14.0659C15.5421 14.457 15.353 14.7492 15.0501 15.0499C14.7472 15.3529 14.4571 15.5419 14.0661 15.6988C12.936 16.1478 10.2526 16.0468 9.0001 16.0468C7.74756 16.0468 5.06202 16.1478 3.93194 15.7009C3.54092 15.5441 3.24873 15.355 2.94795 15.0521C2.64502 14.7492 2.45596 14.4591 2.29913 14.0681C1.85225 12.9359 1.95323 10.2525 1.95323 8.99993C1.95323 7.74739 1.85225 5.06185 2.29913 3.93177C2.45596 3.54075 2.64502 3.24856 2.94795 2.94778C3.25088 2.647 3.54092 2.45579 3.93194 2.29895C5.06202 1.85208 7.74756 1.95306 9.0001 1.95306C10.2526 1.95306 12.9382 1.85208 14.0683 2.29895C14.4593 2.45579 14.7515 2.64485 15.0523 2.94778C15.3552 3.25071 15.5442 3.54075 15.7011 3.93177C16.148 5.06185 16.047 7.74739 16.047 8.99993C16.047 10.2525 16.148 12.9359 15.6989 14.0659Z" fill="#606060"/></svg></a>
										<?php endif; ?>
										<?php if($fb = get_theme_mod('walldi_social_fb')): ?>
										<a href="<?php echo addhttp($fb); ?>" target="_blank" rel="nofollow"><svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M16.9062 0.40625L1.09375 0.40625C0.713477 0.40625 0.40625 0.713477 0.40625 1.09375L0.40625 16.9062C0.40625 17.2865 0.713477 17.5938 1.09375 17.5938L16.9063 17.5937C17.2865 17.5937 17.5938 17.2865 17.5938 16.9062L17.5938 1.09375C17.5938 0.713477 17.2865 0.40625 16.9062 0.40625ZM16.2188 16.2188H12.2635V10.9379H14.4979L14.833 8.34473L12.2635 8.34473V6.68828C12.2635 5.93633 12.4719 5.425 13.5482 5.425L14.9211 5.425V3.10469C14.6826 3.07246 13.8684 3.00156 12.9188 3.00156C10.9379 3.00156 9.58223 4.21113 9.58223 6.43047V8.34258L7.34355 8.34258L7.34355 10.9357H9.58438V16.2188H1.78125L1.78125 1.78125L16.2188 1.78125V16.2188Z" fill="#606060"/></svg></a>
										<?php endif; ?>
									</div>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div id="bottombar">
				WallDi.com 2020 <?php _e('All Rights Reserved', 'walldi'); ?>.
			</div>
		</div>
	</footer>

	<div class="grid">
	    <div class="container">
	        <div class="grid__box">
	            <div class="grid__box-item"></div>
	            <div class="grid__box-item"></div>
	            <div class="grid__box-item"></div>
	            <div class="grid__box-item"></div>
	            <div class="grid__box-item"></div>
	        </div>
	    </div>
	</div>
<?php wp_footer(); ?>
<script>
	document.addEventListener("contextmenu", function(){
		try {
			if (event.target.tagName == "IMG") {
				//alert("in IMG");
				event.cancelBubble = true;
				if(event.preventDefault != undefined) {
					event.preventDefault();
				}
				if(event.stopPropagation != undefined) {
					event.stopPropagation();
				}
				//console.log('in img tag');
				return false;
			} 
		} catch(error) {
			// console.log("NRI error:"+error);
		}
	}, false);
</script>
<?php /* in function.php: jquery_scripts_method() ?>
<script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<?php */ ?>
<?php if(is_front_page()): ?>
<?php /* ?><script>
jQuery(document).bind('mousewheel DOMMouseScroll', function(event) {
  scroll(event);
});

var num = 1;
var scrolling = false;

function scroll(event) {
  event.preventDefault();
  if (!scrolling) {
    scrolling = true;
    if (event.originalEvent.wheelDelta > 0 || event.originalEvent.detail < 0) {
      num--;
      num = num < 1 ? 1 : num;
    } else {
      num++;
      num = num > 5 ? 5 : num;
    }

    jQuery('html, body').animate({
      scrollTop: jQuery('.front_pannel').eq(num-1).offset().top
    }, 500, "linear", function() {
      scrolling = false;
    });
  }
}
</script><?php */ ?>
<?php /* ?><script src="<?php echo get_template_directory_uri(); ?>/js/jquery.scrollify.js"></script>
<script>
	jQuery(document).ready(function(){
		jQuery.scrollify({
			section : ".front_pannel",
			easing: "easeOutExpo",
			scrollSpeed: 1000,
			setHeights: false
		});
	});
</script><?php */ ?>
<?php endif; ?>
<?php wp_footer(); ?>
</body>
</html>